module ApplicationHelper
  def body_tag(&block)
    content_tag :body, class: body_class, &block
  end

  def body_class
    "%s-%s" % [controller_name, action_name]
  end
end
