require "param_constraint"

Rails.application.routes.draw do
  %w(red green blue).each do |color|
    post "", to: "colors\##{color}", constraints: ParamConstraint.new("color", color)
  end

  root to: "colors\#index"
end
