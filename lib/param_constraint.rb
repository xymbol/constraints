class ParamConstraint
  attr_reader :name, :value

  def initialize(name, value)
    @name = name
    @value = value
  end

  def matches?(request)
    request.params[name] == value
  end
end
