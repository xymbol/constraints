require 'test_helper'

class ColorsControllerTest < ActionController::TestCase
  test "get index" do
    get :index
    assert_response :success
  end

  %w(red green blue).each do |color|
    test "post to #{color} with param" do
      post color, color: color
      assert_response :success
    end
  end
end
