require "test_helper"

class ParamConstraintTest < ActiveSupport::TestCase
  def request_for(name, value)
    ActionDispatch::TestRequest.new params: { name => value }
  end

  attr_reader :param_constraint

  setup do
    @param_constraint = ParamConstraint.new "name", "value"
  end

  %w(name value).each do |attribute|
    test "returns #{attribute}" do
      assert_equal attribute, param_constraint.send(attribute)
    end
  end

  test "matching request" do
    skip
    assert param_constraint.matches?(request_for "name", "value")
  end

  test "non matching request" do
    skip
    refute param_constraint.matches?(request_for "name", "other")
  end
end
