require 'test_helper'

class RoutingTest < ActionDispatch::IntegrationTest
  %w(red green blue).each do |color|
    test "routes to #{color} using param" do
      assert_routing({ path: "", method: :post }, { controller: "colors", action: color, color: color }, {}, { color: color })
    end
  end
end
